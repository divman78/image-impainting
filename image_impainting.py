import numpy as np
import cv2
import math
import colorsys
import heapq

#constants

KNOWN = 0
INSIDE = 1
BAND = 2

NBRS = 1 #neighbors of a pixel 
INF = 1e6

def getnbrs(x,y):
	return [(x,y-1),(x-1,y),(x,y+1),(x+1,y)]


def outside(k,l,h,w):
	if(k < 0 or k >= w or l < 0 or l >= h):
		return True
	return False

def initialize(mask, img, mask_img):
	(h, w) = mask.shape
	T = np.full((h, w), INF, dtype=float)
	F = mask.copy()
	narrowband = []
	#print(mask.nonzero())
	#inside_y, inside_x = mask.nonzero()
	inside_x = []
	inside_y = []
	for i in range(h):
		for j in range(w):
			if(mask[i][j] == 1):
				#print(img[i][j])
				inside_x.append(i)
				inside_y.append(j)

	print(len(inside_x))
	cnt = 0
	for ind in range(len(inside_x)):

		i = inside_x[ind]
		j = inside_y[ind]
		#print(mask_img[i][j])
		#cv2.circle(img,(i, j), 5, (0,255,0), -1)
		# if(ind == 1):
		# 	print("hola")
		# 	cv2.circle(img,(i, j), 1, (0,255,0), -1)
			# print(mask[i][j])
			# print(mask[i][j-1])
			# cv2.line(img,(i, j),(i, j-1), (0, 0, 0))
		#print(i,j)
		nbrs = getnbrs(i, j)
		for k, l in nbrs:
			if (outside(k,l,h,w)):
				continue
			if F[k][l] == BAND:
				continue
			if mask[k][l] == 0:
				F[k][l] = BAND
				T[k][l] = 0.0
				#if(cnt == 1):
				#cv2.circle(img,(l, k), 1, (0,255,0), -1)
				print(mask[k][l])
				heapq.heappush(narrowband, (0.0, k, l))
		#print(F[i][j])
				cnt = cnt + 1
	print("cnt = " + str(cnt))

	return (F, T, narrowband)


def gradient(img, mask, F, T, narrowband, i, j):
	(h, w, _) = img.shape
	Tij = T[i][j]

	prev_j = j-1
	next_j = j+1
	if prev_j < 0 or next_j >= h:
		grad_j=INF
	else:
		flag_prev_j = F[i][prev_j]
		flag_next_j = F[i][next_j]

		if (flag_next_j != INSIDE and flag_prev_j != INSIDE):
			grad_j = (T[i][next_j] - T[i][prev_j]) / 2.0
		elif(flag_prev_j != INSIDE):
			grad_j = Tij - T[i][prev_j]
		elif(flag_next_j != INSIDE):
			grad_j = T[i][next_j] - Tij
		else:
			grad_j = 0.0


	prev_i = i-1
	next_i = i+1
	if prev_i < 0 or next_i >= w:
		grad_i=INF
	else:
		flag_prev_i = F[prev_i][j]
		flag_next_i = F[next_i][j]

		if (flag_next_i != INSIDE and flag_prev_i != INSIDE):
			grad_i = (T[next_i][j] - T[prev_i][j]) / 2.0
		elif(flag_prev_i != INSIDE):
			grad_i = Tij - T[prev_i][j]
		elif(flag_next_i != INSIDE):
			grad_i = T[next_i][j] - Tij
		else:
			grad_i = 0.0

	return (grad_i, grad_j)



def solve(img, mask, F, T, narrowband, i1, j1, i2, j2):
	sol = 1.0 * INF
	(h, w, _) = img.shape
	if(outside(i1,j1,h,w) or outside(i2,j2,h,w)):
		return sol
	if(F[i1][j1] == KNOWN):
		if(F[i2][j2] == KNOWN):
			r = math.sqrt(2.0*(T[i1][j1]-T[i2][j2])*(T[i1][j1]-T[i2][j2]))
			s = ((T[i1][j1] + T[i2][j2])*r)/2.0
			if(s >= T[i1][j1] and s >= T[i2][j2]):
				sol = s
			else:
				s += r 
				if(s >= T[i1][j1] and s >= T[i2][j2]):
					sol = s
		else:
			sol = 1 + T[i1][j1]
	elif(F[i2][j2] == KNOWN):
		sol = 1+T[i2][j2]

	return sol


def impaint(img, mask, F, T, narrowband, i, j):
	(h, w, _) = img.shape
	Beps = []
	gradij = gradient(img, mask, F, T, narrowband, i, j)
	for ii in range(-NBRS, NBRS+1):
		for jj in range(-NBRS, NBRS+1):
			nbr_i = i + ii
			nbr_j = j + jj
			if(nbr_i == i and nbr_j == j):
				continue
			Beps.append((nbr_i,nbr_j))

	Ia = (0.0,0.0,0.0)
	s = 0.0
	for k, l in Beps:
		if(outside(k,l,h,w)): #outside
			continue
		if(F[k][l] == INSIDE):
			continue
		r = (float(i-k),float(j-l))
		rlength = math.sqrt(r[0]**2.0 + r[1]**2.0)

		drc = ((r[0]*gradij[0] + r[1]*gradij[1])/rlength)
		dst = 1.0/(rlength*rlength)
		lev = 1.0/(1.0+abs(T[i][j]-T[k][l]))
		weight = drc*dst*lev

		gradI = [(0.0,0.0,0.0), (0.0,0.0,0.0)]
		if(outside(k+1,l,h,w) and outside(k-1,l,h,w) and outside(k,l+1,h,w) and outside(k,l-1,h,w)):
			gradI = [(img[k+1][l][0]-img[k-1][l][0],img[k+1][l][1]-img[k-1][l][1],img[k+1][l][2]-img[k-1][l][2]),(img[l][l+1][0]-img[k][l-1][0],img[k][l+1][1]-img[k][l-1][1],img[k][l+1][2]-img[k][l-1][2])]
		Ia += weight * (img[k][l]+((gradI[0][0]*r[0] + gradI[1][0] * r[1]), (gradI[0][1]*r[0] + gradI[1][1] * r[1]), (gradI[0][2]*r[0] + gradI[1][2] * r[1])))
		s += w
		#Ia = 1.0*img[k][l]
		#s = 1.0
	img[i][j] = Ia/s



def ffm(img, mask, F, T, narrowband):
	(h,w,_) = img.shape
	cnt = 0
	while narrowband:
		cnt = cnt+1
		#if(cnt > INF):
		#	break
		(_, i, j) = heapq.heappop(narrowband)
		#if(cnt == 1):
		#print(i, j)
		F[i][j] = KNOWN
		nbrs = getnbrs(i, j)
		#print(F[i][j])
		#break
		#if(cnt):
			#cv2.line(img,(i, j),(i+1, j+1), (0, 0 ,0 ))
			#cv2.circle( img, 10, 2, (0,0,0), 10, 0, 0 )
			#print(img[i][j])
		for k , l in nbrs:
			if k < 0 or k >= w or l < 0 or l >= h:
				continue
			#print("nbrs : "+str((k, l)) +' '+ str(F[k][l]))
			#break

			if (F[k][l] != KNOWN):
				#print("1helo")
				
				# print(F[k][l])
				if (F[k][l] == INSIDE):
					F[k][l] = BAND
				 	#print("helo")
					#if(cnt == 1):
						#cv2.line(img,(k, l),(k+100, l+100), (0, 0 ,0 ))
					impaint(img, mask, F, T, narrowband, k, l)

				T[k][l] = min(solve(img, mask, F, T, narrowband, k-1,l,k,l-1), solve(img, mask, F, T, narrowband, k+1,l,k,l-1), solve(img, mask, F, T, narrowband, k-1,l,k,l+1), solve(img, mask, F, T, narrowband, k+1,l,k,l+1))

				heapq.heappush(narrowband, (T[k][l], k, l))


        
		#break
	print("sdfs")
	for x in xrange(h):
		for y in xrange(w):
			if(F[k][l] == INSIDE):
				print(k,l)





def do_image_impainting(img, mask_img):
	mask = mask_img[:,:,0:1].copy()
	#print("eheel")
	mask = mask[:,:,0]
	# print(mask)
	mask = (mask > 0)
	# print(mask)
	mask = mask.astype(int)
	print((mask.shape, mask_img.shape))
	#print(mask[212:214, 101:103])
	#filling Flag, T and narrowband priority queue according to T
	(F, T, narrowband) = initialize(mask, img, mask_img)

	#fast forward marching
	ffm(img, mask, F, T, narrowband)

	return img




image_name = './images/lena_in.png'
image_mask_name = './images/lena_mask.png'

# image_name = './images/peppers_in.png'
# image_mask_name = './images/peppers_mask.png'

# image_name = './images/tulips_in.png'
# image_mask_name = './images/tulips_mask.png'


# image_name = './images/veg_in.png'
# image_mask_name = './images/veg_mask_in.png'


image = 	 cv2.imread(image_name)
mask_image = cv2.imread(image_mask_name)

impainted_image = do_image_impainting(image, mask_image)

res_name = "./images/results/"+ image_name.split('.')[0] +"_res" +".jpg"
cv2.imwrite(res_name, impainted_image)

# cv2.imshow('image', impainted_image)
# cv2.waitKey(0)











































import numpy as np
import cv2
import math
import colorsys
import heapq
import copy

#constants

KNOWN = 0
INSIDE = 1
BAND = 2

NBRS = 1 #neighbors of a pixel 
INF = 1e6
EPS = .0000000001

def getnbrs(x,y):
	return [(x,y-1),(x-1,y),(x,y+1),(x+1,y)]


def outside(k,l,h,w):
	if(k < 0 or k >= h or l < 0 or l >= w):
		return True
	return False

def solve(img, mask, F, T, narrowband, i1, j1, i2, j2):
	sol = 1.0 * INF
	(h, w, _) = img.shape
	if(outside(i1,j1,h,w) or outside(i2,j2,h,w)):
		return sol
	if(F[i1][j1] == KNOWN):
		if(F[i2][j2] == KNOWN):
			r = math.sqrt(2.0*(T[i1][j1]-T[i2][j2])*(T[i1][j1]-T[i2][j2]))
			s = ((T[i1][j1] + T[i2][j2])*r)/2.0
			if(s >= T[i1][j1] and s >= T[i2][j2]):
				sol = s
			else:
				s += r 
				if(s >= T[i1][j1] and s >= T[i2][j2]):
					sol = s
		else:
			sol = 1 + T[i1][j1]
	elif(F[i2][j2] == KNOWN):
		sol = 1+T[i2][j2]

	return sol



def gradient(img, F, T, i, j):
	(h, w, _) = img.shape
	Tij = T[i][j]

	prev_j = j-1
	next_j = j+1
	if prev_j < 0 or next_j >= w:
		grad_j=INF
	else:
		flag_prev_j = F[i][prev_j]
		flag_next_j = F[i][next_j]

		if (flag_next_j != INSIDE and flag_prev_j != INSIDE):
			grad_j = (T[i][next_j] - T[i][prev_j]) / 2.0
		elif(flag_prev_j != INSIDE):
			grad_j = Tij - T[i][prev_j]
		elif(flag_next_j != INSIDE):
			grad_j = T[i][next_j] - Tij
		else:
			grad_j = 0.0


	prev_i = i-1
	next_i = i+1
	if prev_i < 0 or next_i >= h:
		grad_i=INF
	else:
		flag_prev_i = F[prev_i][j]
		flag_next_i = F[next_i][j]

		if (flag_next_i != INSIDE and flag_prev_i != INSIDE):
			grad_i = (T[next_i][j] - T[prev_i][j]) / 2.0
		elif(flag_prev_i != INSIDE):
			grad_i = Tij - T[prev_i][j]
		elif(flag_next_i != INSIDE):
			grad_i = T[next_i][j] - Tij
		else:
			grad_i = 0.0

	return (grad_i, grad_j)





def impaint(img, mask, F, T, narrowband, i, j):
	(h, w, _) = img.shape
	Beps = []
	gradij = gradient(img, F, T, i, j)
	for ii in range(-NBRS, NBRS+1):
		for jj in range(-NBRS, NBRS+1):
			nbr_i = i + ii
			nbr_j = j + jj
			if(ii*ii + jj*jj > NBRS):
				continue
			if(nbr_i == i and nbr_j == j):
				continue
			Beps.append((nbr_i,nbr_j))

	Ia = (0.0,0.0,0.0)
	s = 0.0
	Ia1 = (0,0,0)
	mini = INF
	for k, l in Beps:
		if(outside(k,l,h,w)): #outside
			continue
		if(F[k][l] == INSIDE):
			continue
		r = (float(i-k),float(j-l))
		rlength = math.sqrt(r[0]**2.0 + r[1]**2.0)

		drc = ((r[0]*gradij[0] + r[1]*gradij[1])/rlength)
		dst = 1.0/(rlength*rlength)
		lev = 1.0/(1.0+abs(T[i][j]-T[k][l]))
		weight = drc*dst*lev
		if(weight == 0.0):
			weight = EPS
		if(i == 393 and j== 184):
			print("(k,l): " + str((k,l)))
			print("drc : " + str(drc))
			print("lev: " + str(lev))
			print("weight: " + str(weight))
			print("iai = " + str(weight * (img[k][l])))
		gradI = [(0.0,0.0,0.0), (0.0,0.0,0.0)]
		if(weight >= 0.0):
			Ia += weight * (img[k][l])#+((gradI[0][0]*r[0] + gradI[1][0] * r[1]), (gradI[0][1]*r[0] + gradI[1][1] * r[1]), (gradI[0][2]*r[0] + gradI[1][2] * r[1])))
			s += weight
		Ia1 = img[k][l]

	Ias = Ia/s
	if(Ias[0] < 0 or Ias[1] < 0 or Ias[2] < 0):
		img[i][j] = Ia1
	else:
		img[i][j] = Ia/s
	# print(img[i][j])
	if(img[i][j][0] == 244 and img[i][j][1] == 0 and img[i][j][2] == 209):
		print(Ia)
		print(s)
		print(Ia/s)
	if(img[i][j][0] == 244 and img[i][j][1] == 0 and img[i][j][2] == 209):
		print("(i,j) => "+ str((i,j)))
		print("(r,g,b) => ", str(img[i][j]))





def ffm(img, mask, F, T, narrowband):
	(h,w,_) = img.shape
	cnt = 0
	while narrowband:
		cnt = cnt+1
		(_, i, j) = heapq.heappop(narrowband)
		F[i][j] = KNOWN
		nbrs = getnbrs(i, j)
		#print(i, j)
		for k , l in nbrs:
			if (outside(k,l,h,w)):
				continue
			if (F[k][l] != KNOWN):
				if (F[k][l] == INSIDE):
					F[k][l] = BAND
					impaint(img, mask, F, T, narrowband, k, l)
				T[k][l] = min(solve(img, mask, F, T, narrowband, k-1,l,k,l-1), solve(img, mask, F, T, narrowband, k+1,l,k,l-1), solve(img, mask, F, T, narrowband, k-1,l,k,l+1), solve(img, mask, F, T, narrowband, k+1,l,k,l+1))
				heapq.heappush(narrowband, (T[k][l], k, l))


	# for i in range(h):
	# 	for j in range(w):
	# 		if((img[i][j][0] == 209 or img[i][j][1] == 209 or img[i][j][2] == 209) and (img[i][j][0] == 244 or img[i][j][1] == 244 or img[i][j][2] == 244)):
	# 			print("(i,j) => "+ str((i,j)))
	# 			print("(r,g,b) => ", str(img[i][j]))



def initialize(mask, img, mask_img):
	(h, w) = mask.shape
	T = np.full((h, w), INF, dtype=float)
	F = mask.copy()
	narrowband = []
	inside_x = []
	inside_y = []
	for i in range(h):
		for j in range(w):
			if(mask[i][j] == 1):
				##print(img[i][j])
				inside_x.append(i)
				inside_y.append(j)
	cnt = 0
	for ind in range(len(inside_x)):
		i = inside_x[ind]
		j = inside_y[ind]
		nbrs = getnbrs(i, j)
		for k, l in nbrs:
			if (outside(k,l,h,w)):
				continue
			if F[k][l] == BAND:
				continue
			if mask[k][l] == 0:
				F[k][l] = BAND
				T[k][l] = 0.0
				heapq.heappush(narrowband, (0.0, k, l))
				cnt = cnt + 1
	return (F, T, narrowband)




def do_image_impainting(img, mask_img):
	mask = mask_img[:,:,0:1].copy()
	mask = mask[:,:,0]
	mask = (mask > 0)
	mask = mask.astype(int)
	#initializing Flag, T and narrowband priority queue according to T
	(F, T, narrowband) = initialize(mask, img, mask_img)

	#fast forward marching
	ffm(img, mask, F, T, narrowband)

	return img




# image_name = './images/lena_in.png'
# image_mask_name = './images/lena_mask.png'

image_name = './images/peppers_in.png'
image_mask_name = './images/peppers_mask.png'

# image_name = './images/tulips_in.png'
# image_mask_name = './images/tulips_mask.png'


# image_name = './images/veg_in.png'
# image_mask_name = './images/veg_mask_in.png'

# image_name = './images/lincoln11.png'
# image_mask_name = './images/lincoln_mask.png'

# image_name = './images/traffic.jpeg'
# image_mask_name = './images/traffic_mask.jpg'

image_name = './images/ellipses.png'
image_mask_name = './images/ellipses_mask.jpg'

image = 	 cv2.imread(image_name)
mask_image = cv2.imread(image_mask_name)

impainted_image = do_image_impainting(image, mask_image)

res_name = "./images/results/"+ image_name.split('.')[0] +"_res" +".jpg"
cv2.imwrite(res_name, impainted_image)
#print (cv2. __version__)

# cv2.imshow('image', impainted_image)
# cv2.waitKey(0)






















import numpy as np
import cv2
import math
import colorsys
import heapq
import copy

#constants

KNOWN = 0
INSIDE = 1
BAND = 2

NBRS = 1 #neighbors of a pixel 
INF = 1e6
EPS = .0000000001

def getnbrs(x,y):
	return [(x,y-1),(x-1,y),(x,y+1),(x+1,y)]


def outside(k,l,h,w):
	if(k < 0 or k >= h or l < 0 or l >= w):
		return True
	return False

def solve(img, mask, F, T, narrowband, i1, j1, i2, j2):
	sol = 1.0 * INF
	(h, w, _) = img.shape
	if(outside(i1,j1,h,w) or outside(i2,j2,h,w)):
		return sol
	if(F[i1][j1] == KNOWN):
		if(F[i2][j2] == KNOWN):
			r = math.sqrt(2.0*(T[i1][j1]-T[i2][j2])*(T[i1][j1]-T[i2][j2]))
			s = ((T[i1][j1] + T[i2][j2])*r)/2.0
			if(s >= T[i1][j1] and s >= T[i2][j2]):
				sol = s
			else:
				s += r 
				if(s >= T[i1][j1] and s >= T[i2][j2]):
					sol = s
		else:
			sol = 1 + T[i1][j1]
	elif(F[i2][j2] == KNOWN):
		sol = 1+T[i2][j2]

	return sol



def gradient(img, F, T, i, j):
	(h, w, _) = img.shape
	Tij = T[i][j]

	prev_j = j-1
	next_j = j+1
	if prev_j < 0 or next_j >= w:
		grad_j=INF
	else:
		flag_prev_j = F[i][prev_j]
		flag_next_j = F[i][next_j]

		if (flag_next_j != INSIDE and flag_prev_j != INSIDE):
			grad_j = (T[i][next_j] - T[i][prev_j]) / 2.0
		elif(flag_prev_j != INSIDE):
			grad_j = Tij - T[i][prev_j]
		elif(flag_next_j != INSIDE):
			grad_j = T[i][next_j] - Tij
		else:
			grad_j = 0.0


	prev_i = i-1
	next_i = i+1
	if prev_i < 0 or next_i >= h:
		grad_i=INF
	else:
		flag_prev_i = F[prev_i][j]
		flag_next_i = F[next_i][j]

		if (flag_next_i != INSIDE and flag_prev_i != INSIDE):
			grad_i = (T[next_i][j] - T[prev_i][j]) / 2.0
		elif(flag_prev_i != INSIDE):
			grad_i = Tij - T[prev_i][j]
		elif(flag_next_i != INSIDE):
			grad_i = T[next_i][j] - Tij
		else:
			grad_i = 0.0

	return (grad_i, grad_j)





def impaint(img, mask, F, T, narrowband, i, j):
	(h, w, _) = img.shape
	Beps = []
	gradij = gradient(img, F, T, i, j)
	for ii in range(-NBRS, NBRS+1):
		for jj in range(-NBRS, NBRS+1):
			nbr_i = i + ii
			nbr_j = j + jj
			if(ii*ii + jj*jj > NBRS):
				continue
			if(nbr_i == i and nbr_j == j):
				continue
			Beps.append((nbr_i,nbr_j))

	Ia = (0.0,0.0,0.0)
	s = 0.0
	Ia1 = (0,0,0)
	mini = INF
	for k, l in Beps:
		if(outside(k,l,h,w)): #outside
			continue
		if(F[k][l] == INSIDE):
			continue
		r = (float(i-k),float(j-l))
		rlength = math.sqrt(r[0]**2.0 + r[1]**2.0)

		drc = ((r[0]*gradij[0] + r[1]*gradij[1])/rlength)
		dst = 1.0/(rlength*rlength)
		lev = 1.0/(1.0+abs(T[i][j]-T[k][l]))
		weight = drc*dst*lev
		if(weight == 0.0):
			weight = EPS
		if(i == 393 and j== 184):
			print("(k,l): " + str((k,l)))
			print("drc : " + str(drc))
			print("lev: " + str(lev))
			print("weight: " + str(weight))
			print("iai = " + str(weight * (img[k][l])))
		gradI = [(0.0,0.0,0.0), (0.0,0.0,0.0)]
		if(weight >= 0.0):
			Ia += weight * (img[k][l])#+((gradI[0][0]*r[0] + gradI[1][0] * r[1]), (gradI[0][1]*r[0] + gradI[1][1] * r[1]), (gradI[0][2]*r[0] + gradI[1][2] * r[1])))
			s += weight
		Ia1 = img[k][l]

	Ias = Ia/s
	if(Ias[0] < 0 or Ias[1] < 0 or Ias[2] < 0):
		img[i][j] = Ia1
	else:
		img[i][j] = Ia/s
	# print(img[i][j])
	if(img[i][j][0] == 244 and img[i][j][1] == 0 and img[i][j][2] == 209):
		print(Ia)
		print(s)
		print(Ia/s)
	if(img[i][j][0] == 244 and img[i][j][1] == 0 and img[i][j][2] == 209):
		print("(i,j) => "+ str((i,j)))
		print("(r,g,b) => ", str(img[i][j]))





def ffm(img, mask, F, T, narrowband):
	(h,w,_) = img.shape
	cnt = 0
	while narrowband:
		cnt = cnt+1
		(_, i, j) = heapq.heappop(narrowband)
		F[i][j] = KNOWN
		nbrs = getnbrs(i, j)
		#print(i, j)
		for k , l in nbrs:
			if (outside(k,l,h,w)):
				continue
			if (F[k][l] != KNOWN):
				if (F[k][l] == INSIDE):
					F[k][l] = BAND
					impaint(img, mask, F, T, narrowband, k, l)
				T[k][l] = min(solve(img, mask, F, T, narrowband, k-1,l,k,l-1), solve(img, mask, F, T, narrowband, k+1,l,k,l-1), solve(img, mask, F, T, narrowband, k-1,l,k,l+1), solve(img, mask, F, T, narrowband, k+1,l,k,l+1))
				heapq.heappush(narrowband, (T[k][l], k, l))


	# for i in range(h):
	# 	for j in range(w):
	# 		if((img[i][j][0] == 209 or img[i][j][1] == 209 or img[i][j][2] == 209) and (img[i][j][0] == 244 or img[i][j][1] == 244 or img[i][j][2] == 244)):
	# 			print("(i,j) => "+ str((i,j)))
	# 			print("(r,g,b) => ", str(img[i][j]))


def _compute_outside_dists(height, width, dists, flags, band, radius):
    band = band.copy()
    orig_flags = flags
    flags = orig_flags.copy()
    # swap INSIDE / OUTSIDE
    flags[orig_flags == KNOWN] = UNKNOWN
    flags[orig_flags == UNKNOWN] = KNOWN

    last_dist = 0.0
    double_radius = radius * 2
    while band:
        # reached radius limit, stop FFM
        if last_dist >= double_radius:
            break

        # pop BAND pixel closest to initial mask contour and flag it as KNOWN
        _, y, x = heapq.heappop(band)
        flags[y, x] = KNOWN

        # process immediate neighbors (top/bottom/left/right)
        neighbors = [(y - 1, x), (y, x - 1), (y + 1, x), (y, x + 1)]
        for nb_y, nb_x in neighbors:
            # skip out of frame
            if nb_y < 0 or nb_y >= height or nb_x < 0 or nb_x >= width:
                continue

            # neighbor already processed, nothing to do
            if flags[nb_y, nb_x] != UNKNOWN:
                continue

            # compute neighbor distance to inital mask contour
            last_dist = min([
                solve(img, mask, F, T, narrowband, nb_x, nb_y - 1, nb_x - 1,  nb_y),
                solve(img, mask, F, T, narrowband, nb_x, nb_y + 1, nb_x + 1, nb_y),
                solve(img, mask, F, T, narrowband, nb_x, nb_y - 1, nb_x + 1, nb_y),
                solve(img, mask, F, T, narrowband, nb_x, nb_y + 1, nb_x - 1, nb_y)
            ])
            dists[nb_y, nb_x] = last_dist

            # add neighbor to narrow band
            flags[nb_y, nb_x] = BAND
            heapq.heappush(band, (last_dist, nb_y, nb_x))

    # distances are opposite to actual FFM propagation direction, fix it
    dists *= -1.0


def initialize(mask, img, mask_img):
	(h, w) = mask.shape
	T = np.full((h, w), INF, dtype=float)
	F = mask.copy()
	narrowband = []
	inside_x = []
	inside_y = []
	for i in range(h):
		for j in range(w):
			if(mask[i][j] == 1):
				##print(img[i][j])
				inside_x.append(i)
				inside_y.append(j)
	cnt = 0
	for ind in range(len(inside_x)):
		i = inside_x[ind]
		j = inside_y[ind]
		nbrs = getnbrs(i, j)
		for k, l in nbrs:
			if (outside(k,l,h,w)):
				continue
			if F[k][l] == BAND:
				continue
			if mask[k][l] == 0:
				F[k][l] = BAND
				T[k][l] = 0.0
				heapq.heappush(narrowband, (0.0, k, l))
				cnt = cnt + 1
	return (F, T, narrowband)
	#_compute_outside_dists(h, w, T, F, narrowband, NBRS)




def do_image_impainting(img, mask_img):
	mask = mask_img[:,:,0:1].copy()
	mask = mask[:,:,0]
	mask = (mask > 0)
	mask = mask.astype(int)
	#initializing Flag, T and narrowband priority queue according to T
	(F, T, narrowband) = initialize(mask, img, mask_img)

	#fast forward marching
	ffm(img, mask, F, T, narrowband)

	return img




# image_name = './images/lena_in.png'
# image_mask_name = './images/lena_mask.png'

image_name = './images/peppers_in.png'
image_mask_name = './images/peppers_mask.png'

# image_name = './images/tulips_in.png'
# image_mask_name = './images/tulips_mask.png'


# image_name = './images/veg_in.png'
# image_mask_name = './images/veg_mask_in.png'

# image_name = './images/lincoln11.png'
# image_mask_name = './images/lincoln_mask.png'

# image_name = './images/traffic.jpeg'
# image_mask_name = './images/traffic_mask.jpg'

image_name = './images/ellipses.png'
image_mask_name = './images/ellipses_mask.jpg'

image = 	 cv2.imread(image_name)
mask_image = cv2.imread(image_mask_name)

impainted_image = do_image_impainting(image, mask_image)

res_name = "./images/results/"+ image_name.split('.')[0] +"_res" +".jpg"
cv2.imwrite(res_name, impainted_image)
#print (cv2. __version__)

# cv2.imshow('image', impainted_image)
# cv2.waitKey(0)






























import numpy as np
import cv2
import math
import colorsys
import heapq
import copy

#constants

KNOWN = 0
INSIDE = 1
BAND = 2

NBRS = 1 #neighbors of a pixel 
INF = 1e6
EPS = .0000000001

def getnbrs(x,y):
	return [(x,y-1),(x-1,y),(x,y+1),(x+1,y)]


def outside(k,l,h,w):
	if(k < 0 or k >= h or l < 0 or l >= w):
		return True
	return False

def solve(img, mask, F, T, narrowband, i1, j1, i2, j2):
	sol = 1.0 * INF
	(h, w, _) = img.shape
	if(outside(i1,j1,h,w) or outside(i2,j2,h,w)):
		return sol
	if(F[i1][j1] == KNOWN):
		if(F[i2][j2] == KNOWN):
			r = math.sqrt(2.0*(T[i1][j1]-T[i2][j2])*(T[i1][j1]-T[i2][j2]))
			s = ((T[i1][j1] + T[i2][j2])*r)/2.0
			if(s >= T[i1][j1] and s >= T[i2][j2]):
				sol = s
			else:
				s += r 
				if(s >= T[i1][j1] and s >= T[i2][j2]):
					sol = s
		else:
			sol = 1 + T[i1][j1]
	elif(F[i2][j2] == KNOWN):
		sol = 1+T[i2][j2]

	return sol



def gradient(img, F, T, i, j):
	(h, w, _) = img.shape
	Tij = T[i][j]

	prev_j = j-1
	next_j = j+1
	if prev_j < 0 or next_j >= w:
		grad_j=INF
	else:
		flag_prev_j = F[i][prev_j]
		flag_next_j = F[i][next_j]

		if (flag_next_j != INSIDE and flag_prev_j != INSIDE):
			grad_j = (T[i][next_j] - T[i][prev_j]) / 2.0
		elif(flag_prev_j != INSIDE):
			grad_j = Tij - T[i][prev_j]
		elif(flag_next_j != INSIDE):
			grad_j = T[i][next_j] - Tij
		else:
			grad_j = 0.0


	prev_i = i-1
	next_i = i+1
	if prev_i < 0 or next_i >= h:
		grad_i=INF
	else:
		flag_prev_i = F[prev_i][j]
		flag_next_i = F[next_i][j]

		if (flag_next_i != INSIDE and flag_prev_i != INSIDE):
			grad_i = (T[next_i][j] - T[prev_i][j]) / 2.0
		elif(flag_prev_i != INSIDE):
			grad_i = Tij - T[prev_i][j]
		elif(flag_next_i != INSIDE):
			grad_i = T[next_i][j] - Tij
		else:
			grad_i = 0.0

	return (grad_i, grad_j)



def subtract(pixel1, pixel2):
	pixel1 = (float(pixel1[0]),float(pixel1[1]),float(pixel1[2]))
	pixel2 = (float(pixel2[0]),float(pixel2[1]),float(pixel2[2]))
	return ((pixel1[0]-pixel2[0], pixel1[1]-pixel2[1], pixel1[2]-pixel2[2]))



def impaint(img, mask, F, T, narrowband, i, j):
	(h, w, _) = img.shape
	Beps = []
	gradij = gradient(img, F, T, i, j)
	for ii in range(-NBRS, NBRS+1):
		for jj in range(-NBRS, NBRS+1):
			nbr_i = i + ii
			nbr_j = j + jj
			# if(ii*ii + jj*jj > NBRS):
			# 	continue
			if(nbr_i == i and nbr_j == j):
				continue
			Beps.append((nbr_i,nbr_j))

	Ia = (0.0,0.0,0.0)
	s = 0.0
	Ia1 = (0,0,0)
	J = [(0.0,0.0,0.0),(0.0,0.0,0.0)]
	###print("\n")
	mini = INF
	for k, l in Beps:
		if(outside(k,l,h,w)): #outside
			continue
		if(F[k][l] == INSIDE):
			continue
		r = (float(i-k),float(j-l))
		rlength = math.sqrt(r[0]**2.0 + r[1]**2.0)

		drc = abs((r[0]*gradij[0] + r[1]*gradij[1])/rlength)
		dst = 1.0/(rlength*rlength)
		lev = 1.0/(1.0+abs(T[i][j]-T[k][l]))

		weight = drc*dst*lev

		#gradI
		gradI = [(0.0,0.0,0.0),(0.0,0.0,0.0)]
		if(F[k][l+1] != INSIDE):
			if(F[k][l-1] != INSIDE):
				gradI[0] = (subtract(img[k][l+1],img[k][l-1]))
			else:
				gradI[0] = (subtract(img[k][l+1],(0.0,0.0,0.0)))
		else:
			if(F[k][l-1] != INSIDE):
				gradI[0] = (subtract((0.0,0.0,0.0),img[k][l-1]))
			else:
				gradI[0] = (0.0,0.0,0.0)
		if(F[k+1][l] != INSIDE):
			if(F[k-1][l] != INSIDE):
				gradI[1] = (subtract(img[k+1][l],img[k-1][l]))
			else:
				gradI[1] = (subtract(img[k+1][l], (0.0,0.0,0.0)))
		else:
			if(F[k-1][l] != INSIDE):
				gradI[1] = (subtract((0.0,0.0,0.0),img[k-1][l]))
			else:
				gradI[1] = (0.0,0.0,0.0)

		print(J)
		print(gradI)


		J[0] = subtract((J[0][0],J[0][1],J[0][2]), (gradI[0][0]*r[0]*w, gradI[0][1]*r[0]*w,gradI[0][2]*r[0]*w))
		J[1] = subtract((J[1][0],J[1][1], J[1][2]), (gradI[1][0]*r[1]*w,gradI[1][1]*r[1]*w,gradI[1][2]*r[1]*w))

		if(weight == 0.0):
			weight = EPS
		if(i == 393 and j== 184):
			print("(k,l): " + str((k,l)))
			print("drc : " + str(drc))
			print("lev: " + str(lev))
			print("weight: " + str(weight))
			print("iai = " + str(weight * (img[k][l])))
		gradI = [(0.0,0.0,0.0), (0.0,0.0,0.0)]
		if(weight >= 0.0):
			Ia += weight * (img[k][l])#+((gradI[0][0]*r[0] + gradI[1][0] * r[1]), (gradI[0][1]*r[0] + gradI[1][1] * r[1]), (gradI[0][2]*r[0] + gradI[1][2] * r[1])))
			s += weight
		Ia1 = img[k][l]

	Ias = Ia/s
	print(J)
	print(Ias)
	if(Ias[0] < 0.0 or Ias[1] < 0.0 or Ias[2] < 0.0):
		img[i][j] = Ia1
	

	else:
		img[i][j][0] = Ias[0]  + ((J[0][0]+J[1][0])/(math.sqrt(J[0][0]*J[0][0]+J[1][0]*J[1][0]) + EPS)) + 0.5
		img[i][j][1] = Ias[1] + ((J[0][1]+J[1][1])/(math.sqrt(J[0][1]*J[0][1]+J[1][1]*J[1][1]) + EPS)) + 0.5
		img[i][j][2] = Ias[2]  + ((J[0][2]+J[1][2])/(math.sqrt(J[0][2]*J[0][2]+J[1][2]*J[1][2]) + EPS)) + 0.5
	# print(img[i][j])
	if(img[i][j][0] == 244 and img[i][j][1] == 0 and img[i][j][2] == 209):
		print(Ia)
		print(s)
		print(Ia/s)
	if(img[i][j][0] == 244 and img[i][j][1] == 0 and img[i][j][2] == 209):
		print("(i,j) => "+ str((i,j)))
		print("(r,g,b) => ", str(img[i][j]))





def ffm(img, mask, F, T, narrowband):
	(h,w,_) = img.shape
	cnt = 0
	while narrowband:
		cnt = cnt+1
		(_, i, j) = heapq.heappop(narrowband)
		F[i][j] = KNOWN
		nbrs = getnbrs(i, j)
		#print(i, j)
		for k , l in nbrs:
			if (outside(k,l,h,w)):
				continue
			if (F[k][l] != KNOWN):
				if (F[k][l] == INSIDE):
					F[k][l] = BAND
					impaint(img, mask, F, T, narrowband, k, l)
				T[k][l] = min(solve(img, mask, F, T, narrowband, k-1,l,k,l-1), solve(img, mask, F, T, narrowband, k+1,l,k,l-1), solve(img, mask, F, T, narrowband, k-1,l,k,l+1), solve(img, mask, F, T, narrowband, k+1,l,k,l+1))
				heapq.heappush(narrowband, (T[k][l], k, l))


	# for i in range(h):
	# 	for j in range(w):
	# 		if((img[i][j][0] == 209 or img[i][j][1] == 209 or img[i][j][2] == 209) and (img[i][j][0] == 244 or img[i][j][1] == 244 or img[i][j][2] == 244)):
	# 			print("(i,j) => "+ str((i,j)))
	# 			print("(r,g,b) => ", str(img[i][j]))


def _compute_outside_dists(height, width, dists, flags, narrowband, radius):
    band = narrowband.copy()
    orig_flags = flags
    flags = orig_flags.copy()
    # swap INSIDE / OUTSIDE
    flags[orig_flags == KNOWN] = UNKNOWN
    flags[orig_flags == UNKNOWN] = KNOWN

    last_dist = 0.0
    double_radius = radius * 2
    while band:
        # reached radius limit, stop FFM
        if last_dist >= double_radius:
            break

        # pop BAND pixel closest to initial mask contour and flag it as KNOWN
        _, y, x = heapq.heappop(band)
        flags[y, x] = KNOWN

        # process immediate neighbors (top/bottom/left/right)
        neighbors = [(y - 1, x), (y, x - 1), (y + 1, x), (y, x + 1)]
        for nb_y, nb_x in neighbors:
            # skip out of frame
            if nb_y < 0 or nb_y >= height or nb_x < 0 or nb_x >= width:
                continue

            # neighbor already processed, nothing to do
            if flags[nb_y, nb_x] != UNKNOWN:
                continue

            # compute neighbor distance to inital mask contour
            last_dist = min([
                solve(img, mask, F, T, narrowband, nb_x, nb_y - 1, nb_x - 1,  nb_y),
                solve(img, mask, F, T, narrowband, nb_x, nb_y + 1, nb_x + 1, nb_y),
                solve(img, mask, F, T, narrowband, nb_x, nb_y - 1, nb_x + 1, nb_y),
                solve(img, mask, F, T, narrowband, nb_x, nb_y + 1, nb_x - 1, nb_y)
            ])
            dists[nb_y, nb_x] = last_dist

            # add neighbor to narrow band
            flags[nb_y, nb_x] = BAND
            heapq.heappush(band, (last_dist, nb_y, nb_x))

    # distances are opposite to actual FFM propagation direction, fix it
    dists *= -1.0
    return dists


def initialize(mask, img, mask_img):
	(h, w) = mask.shape
	T = np.full((h, w), INF, dtype=float)
	F = mask.copy()
	narrowband = []
	inside_x = []
	inside_y = []
	for i in range(h):
		for j in range(w):
			if(mask[i][j] == 1):
				##print(img[i][j])
				inside_x.append(i)
				inside_y.append(j)
	cnt = 0
	for ind in range(len(inside_x)):
		i = inside_x[ind]
		j = inside_y[ind]
		nbrs = getnbrs(i, j)
		for k, l in nbrs:
			if (outside(k,l,h,w)):
				continue
			if F[k][l] == BAND:
				continue
			if mask[k][l] == 0:
				F[k][l] = BAND
				T[k][l] = 0.0
				heapq.heappush(narrowband, (0.0, k, l))
				cnt = cnt + 1
	return (F, T, narrowband)
	T = _compute_outside_dists(h, w, T, F, narrowband, NBRS)




def do_image_impainting(img, mask_img):
	mask = mask_img[:,:,0:1].copy()
	mask = mask[:,:,0]
	mask = (mask > 0)
	mask = mask.astype(int)
	#initializing Flag, T and narrowband priority queue according to T
	(F, T, narrowband) = initialize(mask, img, mask_img)

	#fast forward marching
	ffm(img, mask, F, T, narrowband)

	return img




# image_name = './images/lena_in.png'
# image_mask_name = './images/lena_mask.png'

image_name = './images/peppers_in.png'
image_mask_name = './images/peppers_mask.png'

# image_name = './images/tulips_in.png'
# image_mask_name = './images/tulips_mask.png'


# image_name = './images/veg_in.png'
# image_mask_name = './images/veg_mask_in.png'

# image_name = './images/lincoln11.png'
# image_mask_name = './images/lincoln_mask.png'

# image_name = './images/traffic.jpeg'
# image_mask_name = './images/traffic_mask.jpg'

image_name = './images/ellipses.png'
image_mask_name = './images/ellipses_mask.jpg'

image = 	 cv2.imread(image_name)
mask_image = cv2.imread(image_mask_name)

impainted_image = do_image_impainting(image, mask_image)

res_name = "./images/results/"+ image_name.split('.')[0] +"_res" +".jpg"
cv2.imwrite(res_name, impainted_image)
#print (cv2. __version__)

# cv2.imshow('image', impainted_image)
# cv2.waitKey(0)













