import numpy as np
import cv2
import math
import colorsys
import heapq
import copy

#calibration factors are NBRS and w, w can be product of any combination of drc, lev, dst


#constants
KNOWN = 0
INSIDE = 1
BAND = 2


NBRS = 3 #neighbors of a pixel, calibrative
INF = 1e6 
EPS = 1.0e-20

def getnbrs(x,y):
	return [(x,y-1),(x-1,y),(x,y+1),(x+1,y)]


def outside(k,l,h,w):
	if(k < 0 or k >= h or l < 0 or l >= w):
		return True
	return False

def solve(img, mask, F, T, narrowband, i1, j1, i2, j2):
	sol = 1.0 * INF
	(h, w, _) = img.shape
	if(outside(i1,j1,h,w) or outside(i2,j2,h,w)):
		return sol
	if(F[i1][j1] == KNOWN):
		if(F[i2][j2] == KNOWN):
			r = math.sqrt(2.0*(T[i1][j1]-T[i2][j2])*(T[i1][j1]-T[i2][j2]))
			s = ((T[i1][j1] + T[i2][j2])*r)/2.0
			if(s >= T[i1][j1] and s >= T[i2][j2]):
				sol = s
			else:
				s += r 
				if(s >= T[i1][j1] and s >= T[i2][j2]):
					sol = s
		else:
			sol = 1 + T[i1][j1]
	elif(F[i2][j2] == KNOWN):
		sol = 1+T[i2][j2]

	return sol




def _compute_outside_dists(height, width, dists, flags, narrowband, radius):
    band = narrowband.copy()
    orig_flags = flags
    flags = orig_flags.copy()
    # swap INSIDE / OUTSIDE
    flags[orig_flags == KNOWN] = UNKNOWN
    flags[orig_flags == UNKNOWN] = KNOWN

    last_dist = 0.0
    double_radius = radius * 2
    while band:
        # reached radius limit, stop FFM
        if last_dist >= double_radius:
            break

        # pop BAND pixel closest to initial mask contour and flag it as KNOWN
        _, y, x = heapq.heappop(band)
        flags[x][y] = KNOWN

        # process immediate neighbors (top/bottom/left/right)
        neighbors = [(y - 1, x), (y, x - 1), (y + 1, x), (y, x + 1)]
        for nb_y, nb_x in neighbors:
            # skip out of frame
            if nb_y < 0 or nb_y >= height or nb_x < 0 or nb_x >= width:
                continue

            # neighbor already processed, nothing to do
            if flags[nb_x][nb_y] != UNKNOWN:
                continue

            # compute neighbor distance to inital mask contour
            last_dist = min([
                solve(img, mask, F, T, narrowband, nb_x, nb_y - 1, nb_x - 1,  nb_y),
                solve(img, mask, F, T, narrowband, nb_x, nb_y + 1, nb_x + 1, nb_y),
                solve(img, mask, F, T, narrowband, nb_x, nb_y - 1, nb_x + 1, nb_y),
                solve(img, mask, F, T, narrowband, nb_x, nb_y + 1, nb_x - 1, nb_y)
            ])
            dists[nb_x][nb_y] = last_dist

            # add neighbor to narrow band
            flags[nb_x][nb_y] = BAND
            heapq.heappush(band, (last_dist, nb_y, nb_x))

    # distances are opposite to actual FFM propagation direction, fix it
    dists *= -1.0
    return dists


def initialize(mask, img, mask_img):
	(h, w) = mask.shape
	T = np.full((h, w), INF, dtype=float)
	F = mask.copy()
	narrowband = []
	inside_x = []
	inside_y = []
	for i in range(h):
		for j in range(w):
			if(mask[i][j] == 1):
				##print(img[i][j])
				inside_x.append(i)
				inside_y.append(j)
	cnt = 0
	for ind in range(len(inside_x)):
		i = inside_x[ind]
		j = inside_y[ind]
		nbrs = getnbrs(i, j)
		for k, l in nbrs:
			if (outside(k,l,h,w)):
				continue
			if F[k][l] == BAND:
				continue
			if mask[k][l] == 0:
				F[k][l] = BAND
				T[k][l] = 0.0
				heapq.heappush(narrowband, (0.0, k, l))
				cnt = cnt + 1
	return (F, T, narrowband)
	#T = _compute_outside_dists(h, w, T, F, narrowband, NBRS)



def TaleaImpaint(img, mask, F, T, narrowband):
	(height, width, _) = img.shape
	i = 0
	j = 0
	ii = 0
	jj = 0
	k = 0
	l = 0
	dist = 0.0

	#util narrowband not empty
	while(narrowband):
		#extract element with minimum T value from narrowband
		(_,ii,jj) = heapq.heappop(narrowband)
		#print(ii, jj)
		F[ii][jj] = KNOWN

		#iterate over straight one unit up, down, left, right from (ii,jj) 
		for q in range(0,4):
			if(q == 0):
				i = ii - 1
				j = jj
			elif(q==1):
				i = ii
				j = jj-1
			elif(q==2):
				i = ii+1
				j = jj
			elif(q==3):
				i=ii
				j=jj+1

			#if (i,j) is outside of frame, do nothing
			if(outside(i,j, height, width)):
				continue

			#if (i,j) is not yet discovered, inpainted
			if(F[i][j] == INSIDE):

				#find T[i][j]
				dist = min(solve(img, mask, F, T, narrowband, i-1,j,i,j-1), solve(img, mask, F, T, narrowband, i+1,j,i,j-1), solve(img, mask, F, T, narrowband, i-1,j,i,j+1), solve(img, mask, F, T, narrowband, i+1,j,i,j+1))
				T[i][j] = dist


				#iterate for every color of three (r,g,b) of (i,j)
				for color in range(0,3):
					gradI = {'x':0.0, 'y':0.0}
					gradT = {'x':0.0, 'y':0.0}
					r =     {'x':0.0, 'y':0.0}
					Ia = 0.0
					Jx = 0.0 #gradI in x 
					Jy = 0.0 #gradI in y
					s = 0.0  #sum of weights
					w = 0.0 
					dst = EPS
					lev = EPS
					drc = EPS
					sat = EPS


					#calculating gradT['x']  
					if(j+1 < width and F[i][j+1] != INSIDE):
						if(j-1 >= 0 and F[i][j-1] != INSIDE):
							gradT['x'] = float(T[i][j+1]-T[i][j-1])
						else:
							gradT['x'] = float(T[i][j+1]-T[i][j])
					else:
						if(j-1>=0 and F[i][j-1] != INSIDE):
							gradT['x'] = float(T[i][j]-T[i][j-1])
						else:
							gradT['x'] = 0.0

					#calculating gradT['x']
					if(i+1 < height and F[i+1][j] != INSIDE):
						if(i-1 >= 0 and F[i-1][j] != INSIDE):
							gradT['y'] = float(T[i+1][j]-T[i-1][j])
						else:
							gradT['y'] = float(T[i+1][j]-T[i][j])
					else:
						if(j-1>=0 and F[i][j-1]!=INSIDE):
							gradT['y'] = float(T[i][j]-T[i][j-1])
						else:
							gradT['y'] = 0.0

					#iterating over all neighbors of (i,j) in radius 'NBRS' and through them calculate intensity of color for (i,j)
					for k in range(i-NBRS, i+NBRS+1):
						km = k#-1 #+ (int(k==1))
						kp = k#-1#- int(k==height-2)
						for l in range(j-NBRS, j+NBRS+1):
							lm = l #+ int(l==1)
							lp = l#-1#-int(l==width-2)
							if(k >= 0 and l >= 0 and k < height and l < width):
								if(F[k][l] != INSIDE and (l-j)*(l-j)+(k-i)*(k-i)<=NBRS*NBRS):


                                 
									r['y'] = float(i-k)
									r['x'] = float(j-l)
									rlength = math.sqrt(r['x']*r['x']+r['y']*r['y'])

									#three parameters
									dst = float(1.0/(rlength*math.sqrt(rlength)))
									lev = float(1.0/(1+abs(T[k][l]-T[i][j])))
									drc = (gradT['x']*r['x'] + gradT['y']*r['y'])
									
									if(abs(drc) <= 0.01):
										drc = EPS

									#w can be product of any combination of these three, hence calibrative
									w = (float)(abs(dst*drc)) #*lev))


									#calculating gradI['x']
									if(l+1 < width and F[k][l+1]!=INSIDE):
										if(l-1 >= 0 and F[k][l-1]!=INSIDE):
											gradI['x'] = float(int(img[km][lp+1][color]) - int(img[km][lm-1][color]))*2.0
										else:
											gradI['x'] = float(int(img[km][lp+1][color]) - int(img[km][lm][color]))
									else:
										if(l-1 >= 0 and F[k][l-1]!=INSIDE):
											gradI['x'] = float(int(img[km][lp][color]) - int(img[km][lm-1][color]))
										else:
											gradI['x'] = 0.0

									#calculating gradI['y']
									if(k+1 < height and F[k+1][l]!=INSIDE):
										if(k-1 >= 0 and F[k-1][l]!=INSIDE):
											gradI['y'] = float(int(img[kp+1][lm][color])-int(img[km-1][lm][color]))*2.0
										else:
											gradI['y'] = float(int(img[kp+1][lm][color])-int(img[km][lm][color]))
									else:
										if(k-1 < height and F[k-1][l]!=INSIDE):
											gradI['y'] = float(int(img[kp][lm][color])-int(img[km-1][lm][color]))
										else:
											gradI['y'] = 0.0

									#summing all neighbor's parameters to their respective variables
									Ia = Ia + float(w) * float(img[km][lm][color])
									Jx = Jx -float(w) *float(gradI['x']*r['x'])
									Jy = Jy - float(w) * float(gradI['y']*r['y'])
									s += w

					#sat is the intensity of color of (i,j) th pixel
					sat = float(Ia/s + (Jx+Jy)/math.sqrt(Jx*Jx+Jy*Jy + EPS) + 0.5)

					img[i][j][color] = sat

				#now that pixel (i,j) is impainted, add it to the band contour to march inward of the inpainting region
				F[i][j] = BAND
				heapq.heappush(narrowband, (T[i][j], i, j))








def do_image_impainting(img, mask_img):
	mask = mask_img[:,:,0:1].copy()
	mask = mask[:,:,0]
	mask = (mask > 0)
	mask = mask.astype(int)

	#initializing Flag, T and narrowband priority queue according to T
	(F, T, narrowband) = initialize(mask, img, mask_img)

	#fast forward marching
	TaleaImpaint(img, mask, F, T, narrowband)

	return img




# image_name = './images/lena_in.png'
# image_mask_name = './images/lena_mask.png'

image_name = './images/peppers_in.png'
image_mask_name = './images/peppers_mask.png'

image_name = './images/tulips_in.png'
image_mask_name = './images/tulips_mask.png'


image_name = './images/veg_in.png'
image_mask_name = './images/veg_mask_in.png'

# image_name = './images/lincoln11.png'
# image_mask_name = './images/lincoln_mask.png'

# image_name = './images/traffic.jpeg'
# image_mask_name = './images/traffic_mask.jpg'

image_name = './images/ellipses.png'
image_mask_name = './images/ellipses_mask.jpg'

# image_name = './images/woman_in__.jpg'
# image_mask_name = './images/mask_woman_in.jpg'

image = 	 cv2.imread(image_name)
mask_image = cv2.imread(image_mask_name)

impainted_image = do_image_impainting(image, mask_image)

res_name = "./images/results/"+ image_name.split('.')[0] +"_res" +".jpg"
cv2.imwrite(res_name, impainted_image)



#print (cv2. __version__)

# cv2.imshow('image', impainted_image)
# cv2.waitKey(0)













